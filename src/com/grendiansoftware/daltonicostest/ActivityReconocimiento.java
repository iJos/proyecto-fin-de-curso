package com.grendiansoftware.daltonicostest;

import java.io.IOException;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Paint.Style;
import android.hardware.Camera;


public class ActivityReconocimiento extends Activity implements SurfaceHolder.Callback {
	 Camera camara;
	 SurfaceView surfaceView;
	 SurfaceHolder surfaceHolder;
	 boolean camaraActiva = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reconocimiento);
		
		getWindow().setFormat(PixelFormat.UNKNOWN);
		
		 surfaceView = (SurfaceView)findViewById(R.id.vistaCamara);
		 surfaceHolder = surfaceView.getHolder();
		 surfaceHolder.addCallback(this);	
	}

	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_reconocimiento, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		case R.id.CamaraON:
				enciendeCamara();
			return true;
		case R.id.CamaraOFF:
				apagaCamara();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	public void enciendeCamara(){
		if(!camaraActiva){//Activo camara
			camara = Camera.open();
			if(camara != null){//Si camara no es nula
				try{
					surfaceView.setBackgroundColor(Color.TRANSPARENT);
					camara.setPreviewDisplay(surfaceHolder);
					camara.setDisplayOrientation(90);
					camara.startPreview();
					camaraActiva = true;	
				}catch(IOException e){
					
				}
			}
		}
	}
	
	
	public void apagaCamara(){
		if(camara != null && camaraActiva){
			 camara.stopPreview();
		     camara.release();
		     camara = null;
		     
		     surfaceView.setBackgroundColor(Color.BLACK);
		     
		     camaraActiva = false;
		}
	}
	
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}	
	
	

	
}
	




